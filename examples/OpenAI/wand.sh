#!/bin/bash

read -r prompt

# Set the OpenAI API key
API_KEY=$OPENAI_API_KEY

# Set the API endpoint
API_ENDPOINT="https://api.openai.com/v1/chat/completions"

# Set the request data
DATA='{
    "model": "gpt-4",
    "messages": [
        {
            "role": "user",
            "content": "Write me a bash script that '$prompt'"
        }
    ],
    "temperature": 1,
    "top_p": 1,
    "n": 1,
    "stream": false,
    "max_tokens": 250,
    "presence_penalty": 0,
    "frequency_penalty": 0
}'

# Send the API request
RESPONSE=$(curl -X POST -s -H "Content-Type: application/json" -H "Authorization: Bearer ${API_KEY}" -d "${DATA}" "${API_ENDPOINT}")

# Extract the generated text from the response
GENERATED_TEXT=$(echo "${RESPONSE}" | jq -r '.choices[0].message.content')

# Print the generated text
echo "${GENERATED_TEXT}"