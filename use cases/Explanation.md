# Explanations

Because language models operate around the idea of semantic similarity, they are often excellent at explaining concepts.

## Code explanation
Feed GPT a script and ask it to explain how it works, it does a fantastic job.