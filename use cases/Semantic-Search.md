# Semantic Search

Semantic search is powered by embeddings and allows you to search for text with "similar" meaning.  
This video does a great job explaining how Redis have approached it with [Redisearch](https://www.youtube.com/watch?v=g2bNHLeKlAg)