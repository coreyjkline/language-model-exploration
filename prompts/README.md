# Prompts

## Project prompts
Any prompts used for generating content that lives in this project should be stored under `prompts/project/`

## Prompt Engineering
Prompt engineering is a field emerging alongside Language models as a primary method of providing context.

### templating
In projects that use language models, you will often see stubs or templates that user input is inserted into in order to provide a certain response.
