# Embeddings
An embedding, or vector, is a numerical representation of the meaning of a piece of text specific to the model.
Embeddings can be used to compare the similarity of ideas.

## OpenAI documentation
See OpenAI documentation for usage and a more in depth explanation
https://platform.openai.com/docs/guides/embeddings/what-are-embeddings
