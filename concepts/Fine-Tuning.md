# Fine-Tuning
Fine-tuning refers to further training a published model on a set of data.

An example of this is in order to give a model with an understanding of language a deeper understanding on specific topics.

Whereas prompts provide ephemeral context to a model, a fine-tuning can produce a new model with more permanent understanding of ideas.

https://platform.openai.com/docs/guides/fine-tuning

## Domain specific
If you have a library or framework that is not open source, and language models have no understanding on, you can 
actually fine tune the model with its details, and then ask question pertaining to it.

This has advantages of prompt context, as it is permanent, and you are often not as constricted in the amount of context
you can provide.

## Drawbacks

### Difficulty
It is sometimes difficult to fine tune models in an effective manor.

### Loss
It is possible to lose coherence of the model through over training.