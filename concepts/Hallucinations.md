# Hallucinations

## GPT-4 explanation
[prompt](../prompts/project/concepts/hallucinations.txt)

Language model hallucinations refer to instances where a language model generates text that appears coherent and contextually relevant but is factually incorrect, misleading, or entirely fictional. These hallucinations occur because language models, during their training, learn to generate text by predicting the most likely next word or token based on the given context. However, they may not have a deep understanding of the content or the ability to verify the factual accuracy of the information they generate.

Hallucinations can occur for various reasons:

- Lack of factual grounding: Language models are not explicitly trained to verify the factual accuracy of their outputs. Their primary goal is to generate text that is fluent and contextually relevant. As a result, they might generate text that seems plausible but is not factually correct.

- Model limitations: While language models can learn a lot from their training data, they are still limited by the inherent statistical nature of their learning process. This limitation can lead to outputs that are partially or entirely incorrect.

- Overfitting or memorization: Language models can sometimes memorize certain incorrect or misleading patterns present in the training data, causing them to generate hallucinations that seem plausible but are factually incorrect.

- Ambiguity: When faced with ambiguous or incomplete input, language models may generate text that fills in missing information or makes assumptions based on the patterns they have learned. This can lead to the generation of hallucinations that do not accurately represent the intended context or information.

Addressing hallucinations in language models is an ongoing area of research, with efforts focusing on improving model training, incorporating external knowledge sources, and refining model outputs through post-processing techniques.


## Bug vs Feature
Hallucinations are considered by some as an error in language model output, and as a feature by others.

### Team Bug
Hallucinations can be critical errors in domain specific models meant to give 100% accurate output.

here is an article from [IEEE](https://spectrum.ieee.org/ai-hallucination#toggle-gdpr)

### Team Feature
The reason it may be considered a feature is that language models may not be intended as informational stores, and as such should not be expected to produce 100% accurate references.
So long as the completions have sufficient semantic accuracy, it has done its job.
As it relates to general models, I personally agree with this and believe that any effective AGI implementations should include native knowledge lookup as a sort of working memory.
