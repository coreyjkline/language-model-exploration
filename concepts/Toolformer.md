# Toolformer
Toolformer is an incredibly powerful concept introduced by MetaAI research (I'm certain the idea has occured to others 
before, but for now I'll give them credit). for a full overview, read the [paper here](https://arxiv.org/abs/2302.04761)
[github](https://github.com/conceptofmind/toolformer)

## Synopsis
Toolformer is the idea that language models can teach themselves how to use tools.

This is often accomplished through teaching the language model to embed "API calls" into its own output and then 
invoking an outside tool during post-processing.  Fine tuned language models have even been shown to understand nesting 
API calls

example with nested calls (I wrote this, it is not a real convo with a language model)
```
prompt: "Find the length of the hypotenuse of a right triangle whose other sides are 3 and 4 inches respectively"

output: "The hypotenuse of a right triangle with sides measuring 3 and 4 inches is 
<API>Calculator(Calculator(Calculator(3,"square"), "+", Calculator(4,"square"))"sqrt")</API> inches"

post-processing output: "The hypotenuse of a right triangle with sides measuring 3 and 4 inches is 5 inches"
```
Because the language model is familiar with the pythagorean theorem and has been provided a calculator, it is able to
utilise an external tool more suited to solving the task than itself.


## implementations
While the initial proposition of toolformer is through fine-tuning, there are some implementations that have found 
success in prompt engineering to achieve similar results.

### OpenAI Plugins
OpenAI has introduced [plugins](https://platform.openai.com/docs/plugins/introduction) which are in fact their 
toolformer implementation.
OpenAI's plugins are a very ambitious attempt to have language models understand how to use any API defined within the
[OpenAPI spec](https://www.openapis.org/).

Currently, OpenAI's plugins operate using prompt-injection and are restricted to limited use and development.

I asked an OpenAI developer on twitter if they would support fine tuned models, but received no reply :(.

### Toolformer-zero
TOolformer-zero is a project I came accross that I thought provided an interesting approach to defining toolformer 
usage.  Check out the project on [github](https://github.com/minosvasilias/toolformer-zero)