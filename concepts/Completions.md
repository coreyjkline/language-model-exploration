# Completions
Completions refer to the output produced by language models.

## GPT-4 explanation
[prompt](../prompts/project/concepts/completions.txt)

Language models are typically trained on a large amount of text data and are designed to predict the probability of the next word in a given sentence or sequence of words. When a language model is given a prompt or partial sentence, it uses its learned knowledge to generate one or more possible continuations or completions of the prompt.

These completions are called "completions" because they are the model's attempt to finish the given prompt in a way that is consistent with the patterns and structures it has learned from its training data. The language model's output is essentially completing the prompt by suggesting one or more possible ways to continue the given sentence or sequence of words.

So, the term "completion" is used because the language model's output is completing the prompt by generating a probable continuation that follows the patterns learned from the training data.