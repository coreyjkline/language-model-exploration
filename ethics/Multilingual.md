# Multilingual
Most of the best performing models currently are only intended for use in English.
I believe that this is a dangerous practice and an effort should be made to produce more multilingual models to prevent non-english speakers from being disenfranchised.

This is largely a result of the training data used for these models being in english, see [the pile](https://github.com/EleutherAI/the-pile)
